package com.rhr.gestiontickets;

import com.rhr.gestiontickets.entites.Status;
import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.services.TicketService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class TicketServiceTest {

    @Autowired
    private TicketService ticketService;


    @Test
    public void testGetAllTickets() {
        List<Ticket> ticketList = ticketService.getAllTickets();
        assertNotNull(ticketList);
    }

    @Test
    public void testGetTicketById() {
        long ticketId = 123;
        Optional<Ticket> ticket = ticketService.getTicketById((ticketId));
        assertNotNull(ticket);
        ticket.ifPresent(value -> assertEquals(ticketId, value.getId()));
    }

    @Test
    public void testSaveTicket() {
        Ticket ticket = new Ticket();
        ticket.setTitre("REF01");
        ticket.setDescription("refrecence ticket O1");
        ticket.setStatus(Status.EN_COURS);
        Ticket ticketSave = ticketService.saveTicket(ticket);
        assertNotNull(ticketSave);
    }

    @Test
    public void testUpdateTicket() {
        long ticketId = 123;
        Ticket ticket = new Ticket();
        ticket.setTitre("REF01 modifier");
        ticket.setDescription("refrecence ticket O1");
        ticket.setStatus(Status.TERMINE);
        Ticket ticketUpdate = ticketService.updateTicket(ticketId, ticket);
        assertNotNull(ticketUpdate);

    }

    @Test
    public void testAssignedTicketToUser() {
        long ticketId = 123;
        long userId = 1;
        String ticketAssigned = ticketService.assignedTicketToUser(ticketId, userId);
        assertNotNull(ticketAssigned);

    }

    @Test
    public void testDeleteTicket() {
        ticketService.deleteTicket(123);
    }

}