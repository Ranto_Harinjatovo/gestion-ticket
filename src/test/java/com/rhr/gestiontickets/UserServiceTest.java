package com.rhr.gestiontickets;

import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.entites.User;
import com.rhr.gestiontickets.services.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;


    @Test
    public void testGetAllUsers() {
        List<User> userList = userService.getAllUsers();
        assertNotNull(userList);
    }

    @Test
    public void testGetTicketAssignedToUser() {
        String userId = "1";
        List<Ticket> ticketList = userService.getTicketAssignedToUser(userId);
        assertNotNull(ticketList);
    }

    @Test
    public void testSaveUser() {
        User user = new User(null, "Tata", "tata@gmail.com");
        User userSave = userService.saveUser(user);
        assertNotNull(userSave);

    }

    @Test
    public void testUpdateUser() {
        long userId = 1;
        User user = new User(1L, "Titi", "titi@gmail.com");
        User userUpdate = userService.updateUser(userId, user);
        assertNotNull(userUpdate);
    }


}
