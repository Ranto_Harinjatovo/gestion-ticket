package com.rhr.gestiontickets.repositories;

import com.rhr.gestiontickets.entites.UserTickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersTicketRepository extends JpaRepository<UserTickets,Long> {
    @Query("SELECT ut FROM UserTickets ut WHERE ut.userId = :userId ")
    List<UserTickets> findAllTicketAssignedToUser(@Param("userId") String userId);

    @Query("SELECT ut FROM UserTickets ut WHERE ut.ticketId = :ticketId ")
    UserTickets findTicketAssignedUser(@Param("ticketId") String ticketId);

    @Modifying
    @Query("delete from UserTickets ut where ut.ticketId=:ticketId")
    void deleteTicketToUser(@Param("ticketId") String ticketId);
}
