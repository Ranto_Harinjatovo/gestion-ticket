package com.rhr.gestiontickets.repositories;

import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.entites.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Ticket findByTitre(String titre);
}
