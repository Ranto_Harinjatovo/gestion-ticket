package com.rhr.gestiontickets.controllers;

import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.entites.User;
import com.rhr.gestiontickets.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
public class UserController {

    private UserService userService;

    @Tag(name = "Services", description = "Gestion  utilisateur")
    @Operation(summary = "lire utilisateur", description = "Récupérer tous les utilisateurs")
    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @Operation(summary = "lire ticket d'une utilisateur", description = "Récupérer les tickets assignés à l'utilisateur")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Ticket.class))}),
            @ApiResponse(responseCode = "404", description = "Ticket pas trouvé",
                    content = @Content)})
    @GetMapping("/users/{id}/ticket")
    public List<Ticket> getTicketAssignedToUser(@PathVariable(name = "id") String id) {
        return userService.getTicketAssignedToUser(id);
    }

    @Operation(summary = "création", description = "Créer un utilisateur")
    @PostMapping("/users")
    public User saveUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @Operation(summary = "modification", description = "Modifier un utilisateur")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Ticket.class))}),
            @ApiResponse(responseCode = "404", description = "Utilisateur pas trouvé",
                    content = @Content)})
    @PutMapping("/users/{id}")
    public User updateUser(@PathVariable long id, @RequestBody User user) {
        return userService.updateUser(id, user);
    }


}
