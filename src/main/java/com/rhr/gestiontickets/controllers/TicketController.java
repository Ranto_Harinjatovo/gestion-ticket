package com.rhr.gestiontickets.controllers;

import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.services.TicketService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class TicketController {

    private TicketService ticketService;

    @Tag(name = "Services", description = "Gestion ticket")
    @Operation(summary = "lire ticket", description = "Récupérer tous les tickets.")
    @GetMapping("/tickets")
    public List<Ticket> getAllTickets() {
        return ticketService.getAllTickets();
    }

    @Operation(summary = "lire ticket", description = "Récupérer un ticket par son ID.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Ticket.class))}),
            @ApiResponse(responseCode = "404", description = "Ticket pas trouvé",
                    content = @Content)})
    @GetMapping("/tickets/{id}")
    public Ticket getTickeById(@PathVariable(name = "id") String id) {
        Optional<Ticket> ticket = ticketService.getTicketById(Long.valueOf(id));
        return ticket.orElse(null);
    }

    @Operation(summary = "Création", description = "Créer un nouveau ticket.")
    @PostMapping("/tickets")
    public Ticket saveTicket(@RequestBody Ticket ticket) {
        return ticketService.saveTicket(ticket);
    }

    @Operation(summary = "Modification", description = "Mettre à jour un ticket existant.")
    @PutMapping("/tickets/{id}")
    public Ticket updateTicket(@PathVariable long id, @RequestBody Ticket ticket) {
        return ticketService.updateTicket(id, ticket);
    }

    @Operation(summary = "Assigner", description = "Assigner un ticket à un utilisateur.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Ticket.class))}),
            @ApiResponse(responseCode = "404", description = "Ticket ou utilisateur  pas trouvé",
                    content = @Content)})
    @PutMapping("/tickets/{id}/assign/{userId}")
    public String assignedTicketToUser(@PathVariable long id, @PathVariable long userId) {
        return ticketService.assignedTicketToUser(id, userId);
    }

    @Operation(summary = "Supprimer", description = "Supprimer un ticket par son ID.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Ticket.class))}),
            @ApiResponse(responseCode = "404", description = "Ticket pas trouvé",
                    content = @Content)})
    @DeleteMapping("/tickets /{id}")
    public String deleteTicket(@PathVariable long id) {
        ticketService.deleteTicket(id);
        return "Le ticket " + id + " a été suprrimer avec succé";
    }
}
