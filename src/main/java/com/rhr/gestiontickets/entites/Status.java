package com.rhr.gestiontickets.entites;

public enum Status {
    EN_COURS,
    TERMINE,
    ANNULE
}
