package com.rhr.gestiontickets.services;

import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.entites.User;
import com.rhr.gestiontickets.entites.UserTickets;
import com.rhr.gestiontickets.repositories.TicketRepository;
import com.rhr.gestiontickets.repositories.UserRepository;
import com.rhr.gestiontickets.repositories.UsersTicketRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class UserService {

    private UserRepository userRepository;
    private TicketRepository ticketRepository;
    private UsersTicketRepository usersTicketRepository;


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    public List<Ticket> getTicketAssignedToUser(String userId) {
        List<Ticket> ticketList = new ArrayList<>();
        //recuperer liste tickeId
        List<UserTickets> userTicketsList = usersTicketRepository.findAllTicketAssignedToUser(userId);

        //recupere les ticket
        for (UserTickets userTicket : userTicketsList) {
            Optional<Ticket> ticket = ticketRepository.findById(Long.valueOf(userTicket.getTicketId()));
            ticket.ifPresent(ticketList::add);
        }
        return ticketList;

    }


    public User saveUser(User user) {
        User userFind = userRepository.findByUsername(user.getUsername());
        if (userFind != null) throw new RuntimeException("cet utilisateur existe déjà");
        return userRepository.save(user);
    }


    public User updateUser(long id, User user) {
        Optional<User> updateUser = userRepository.findById(id);
        if (updateUser.isEmpty()) throw new RuntimeException("L'utilisateur avec ID: [" + id + "] n'est pas trouvé");

        updateUser.get().setEmail(user.getEmail());
        updateUser.get().setUsername(user.getUsername());
        return userRepository.save(updateUser.get());

    }


}
