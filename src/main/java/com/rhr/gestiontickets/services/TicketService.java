package com.rhr.gestiontickets.services;

import com.rhr.gestiontickets.entites.Ticket;
import com.rhr.gestiontickets.entites.User;
import com.rhr.gestiontickets.entites.UserTickets;
import com.rhr.gestiontickets.repositories.TicketRepository;
import com.rhr.gestiontickets.repositories.UserRepository;
import com.rhr.gestiontickets.repositories.UsersTicketRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class TicketService {

    private TicketRepository ticketRepository;
    private UserRepository userRepository;
    private UsersTicketRepository usersTicketRepository;

    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    public Optional<Ticket> getTicketById(Long id) {
        Optional<Ticket> ticket = ticketRepository.findById(id);
        if (ticket.isEmpty()) throw new RuntimeException("ticket avec ID: [" + id + "] n'est pas trouvé");
        return ticket;

    }

    public Ticket saveTicket(Ticket ticket) {
        Ticket ticketFind = ticketRepository.findByTitre(ticket.getTitre());
        if (ticketFind != null) throw new RuntimeException("ce titre existe déjà");
        return ticketRepository.save(ticket);
    }


    public Ticket updateTicket(long id, Ticket ticket) {
        Optional<Ticket> ticketUpdate = ticketRepository.findById(id);
        if (ticketUpdate.isEmpty()) throw new RuntimeException("Le ticket avec ID: [" + id + "] n'est pas trouvé");

        ticketUpdate.get().setDescription(ticket.getDescription());
        ticketUpdate.get().setTitre(ticket.getTitre());
        ticketUpdate.get().setStatus(ticket.getStatus());
        return ticketRepository.save(ticketUpdate.get());

    }


    public String assignedTicketToUser(long ticketId, long userId) {
        //verifier le ticket s'il est existe dans le base
        Optional<Ticket> ticket = ticketRepository.findById(ticketId);
        //si vide renvoie une exception
        if (ticket.isEmpty()) throw new RuntimeException("Le ticket " + ticketId + " n'est pas trouvé.");
        //verifier l'utilisateur s'il est existe dans le base
        Optional<User> user = userRepository.findById(userId);
        //si vide renvoie une exception
        if (user.isEmpty()) throw new RuntimeException("L'utilisateur " + userId + " n'est pas trouvé.");

        //verifier si le ticket est déjà affecter  à un utilisateur
        UserTickets userTickets = usersTicketRepository.findTicketAssignedUser(String.valueOf(ticketId));
        //si oui renvoie une exception
        if (userTickets != null)
            throw new RuntimeException("Le ticket " + ticket.get().getTitre() + "est déjà affecté au user " + user.get().getUsername());
        //sinon affecter le ticket au utilisateur
        UserTickets userTicketSave = UserTickets.builder()
                .userId(String.valueOf(userId))
                .ticketId(String.valueOf(ticketId)).build();
        usersTicketRepository.save(userTicketSave);
        return "Le ticket " + ticket.get().getTitre() + " est bien affecté au user " + user.get().getUsername() + ".";

    }


    public void deleteTicket(long id) {
        Optional<Ticket> ticketUpdate = ticketRepository.findById(id);
        if (ticketUpdate.isEmpty()) throw new RuntimeException("Le ticket avec ID: [" + id + "] n'est pas trouvé");
        ticketRepository.deleteById(id);
        usersTicketRepository.deleteTicketToUser(String.valueOf(id));

    }
}
