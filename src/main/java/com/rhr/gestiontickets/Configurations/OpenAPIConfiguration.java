package com.rhr.gestiontickets.Configurations;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
public class OpenAPIConfiguration {
    @Bean
    public OpenAPI defineOpenApi() {
        Server server = new Server();
        server.setUrl("http://localhost:8080");
        server.setDescription("API");

        Contact myContact = new Contact();
        myContact.setName("Ranto");
        myContact.setEmail("ranto.odissi@gmail.com");

        Info information = new Info()
                .title("Gestion des tickets API")
                .version("1.0")
                .description("Cet API permet de gérer les tickets.")
                .contact(myContact);
        return new OpenAPI().info(information).servers(List.of(server));
    }
}
